# interface.py
# Generated from c:\programowanko\Pajtonowe rzeczy\markov\interface.ui automatically by PhoPyQtClassGenerator VSCode Extension
from PyQt5 import uic
from PyQt5.QtWidgets import QWidget
from markov import markov_sentence_generator


class myWidget(QWidget):
	def __init__(self, parent=None):
		super().__init__(parent=parent) # Call the inherited classes __init__ method
		self.ui = uic.loadUi("interface.ui", self) # Load the .ui file

		self.initUI()
		self.show() # Show the GUI


	def initUI(self):
		self.ui.pushButton.clicked.connect(self.startMarkov)

	def startMarkov(self):

		text = self.ui.textEdit.toPlainText()
		if len(text.split()) < 2:
			self.ui.textEdit_2.setText("")
		else:
			generated_text = markov_sentence_generator(text, 
				self.ui.wordNumber.value(),
				self.ui.sentenceNumber.value()
				)
			self.ui.textEdit_2.setText(generated_text)