import random

def markov_sentence_generator(source_text, length, n_sentences=1):
    words = []

    for s in source_text.split():
        s1 = s
        for t in s:
            if not t.isalnum():
                s1 = s1.replace(t, '')
        s1 = s1.lower()
        words.append(s1)

    markov_matrix = {}

    for x in range(0, len(words)-1):
        if words[x] not in markov_matrix:
            markov_matrix[words[x]] = dict([(words[x+1], 1)])
        else:
            if words[x+1] in markov_matrix[words[x]]:
                number = markov_matrix[words[x]][words[x+1]]
                markov_matrix[words[x]][words[x+1]] = number + 1
            else:
                markov_matrix[words[x]][words[x+1]] = 1

    f = list(set(words))

    result = ""

    for _ in range(n_sentences):
        a = random.randint(0, len(f)-1)
        last_word = f[a]

        sentence = last_word
        sentence = sentence[0].upper() + sentence[1:]

        for x in range(1, length):
            try:
                posibilities = markov_matrix[last_word]

                sum_ = sum(posibilities.values())

                for k,v in zip(posibilities.keys(), posibilities.values()):
                    sum_ = sum_ - v
                    if sum_ <= 0:
                        last_word = k

            except Exception:
                a = random.randint(0, len(f)-2)
                last_word = f[a]
            
            sentence = sentence + " " + last_word

        result = result + sentence + ". "

    return result